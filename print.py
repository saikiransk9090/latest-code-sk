import requests
import pymsteams

# Function to fetch incidents in triggered state from PagerDuty
def get_triggered_incidents(api_key, team_ids):
    headers = {
        "Authorization": f"Token token={api_key}",
        "Content-Type": "application/json",
    }
    incidents = []
    for team_id in team_ids:
        response = requests.get(
            f"https://api.pagerduty.com/incidents?team_ids[]={team_id}&statuses[]=triggered",
            headers=headers,
        )
        if response.status_code == 200:
            data = response.json()
            incidents.extend(data['incidents'])
        else:
            print(f"Failed to fetch incidents for team {team_id}: {response.status_code}")
    return incidents

# Function to send a message to a Microsoft Teams channel
def send_to_teams(webhook_url, incidents):
    if not incidents:
        message = "No triggered incidents found."
    else:
        message = "Triggered Incidents:\n"
        for incident in incidents:
            message += f"Incident ID: {incident['id']}, Title: {incident['title']}, Status: {incident['status']}\n"
    
    teams_message = pymsteams.connectorcard(webhook_url)
    teams_message.text(message)
    teams_message.send()

# Main code
pagerduty_api_key = "your_pagerduty_api_key"
team_ids = ["team_id1", "team_id2"]  # Replace with your team IDs
teams_webhook_url = "your_teams_webhook_url"

incidents = get_triggered_incidents(pagerduty_api_key, team_ids)
send_to_teams(teams_webhook_url, incidents)
