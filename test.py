import requests
from datetime import datetime, timedelta

# Function to fetch open incidents from PagerDuty
def get_open_incidents(api_token, team_id):
    url = f"https://api.pagerduty.com/incidents"
    headers = {
        "Accept": "application/vnd.pagerduty+json;version=2",
        "Authorization": f"Token token={api_token}"
    }
    params = {
        "team_ids[]": team_id,
        "statuses[]": "triggered",
        "since": (datetime.now() - timedelta(days=1)).isoformat(),
        "until": datetime.now().isoformat()
    }

    response = requests.get(url, headers=headers, params=params)
    if response.status_code == 200:
        return response.json()["incidents"]
    else:
        print("Failed to fetch open incidents:", response.status_code)
        return []

# Function to send a message to Microsoft Teams
def send_to_teams(webhook_url, message):
    payload = {
        "text": message
    }
    response = requests.post(webhook_url, json=payload)
    if response.status_code == 200:
        print("Message sent successfully to Teams.")
    else:
        print("Failed to send message to Teams:", response.status_code)

# Replace with your PagerDuty API token and team ID
pagerduty_api_token = "YOUR_PAGERDUTY_API_TOKEN"
pagerduty_team_id = "YOUR_PAGERDUTY_TEAM_ID"

# Replace with your Microsoft Teams webhook URL
teams_webhook_url = "YOUR_TEAMS_WEBHOOK_URL"

# Get open incidents from PagerDuty
open_incidents = get_open_incidents(pagerduty_api_token, pagerduty_team_id)

# Create a list of incident summaries
incident_list = "\n".join([f"- {incident['summary']}" for incident in open_incidents])

# Send the incident list to Microsoft Teams
if incident_list:
    send_to_teams(teams_webhook_url, f"Open Incidents on PagerDuty:\n{incident_list}")
else:
    print("No open incidents to report.")
